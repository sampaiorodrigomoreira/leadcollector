package br.com.lead.collector.repositories;

import br.com.lead.collector.models.Produto;
import org.springframework.data.repository.CrudRepository;

//precisamos passar a entidade(tabela) e o tipo da chave primaria Integer
public interface ProdutoRepository extends CrudRepository<Produto, Integer>{}

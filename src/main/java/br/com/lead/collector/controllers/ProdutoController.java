package br.com.lead.collector.controllers;

import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.LeadService;
import br.com.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/produtos")   //url amigavel
public class ProdutoController {

    @Autowired      //injecao de dependencia
    private ProdutoService produtoService;

    @PostMapping    //cadastrar algo novo no banco de dados
    @ResponseStatus(HttpStatus.CREATED)
    public Produto cadastrarProduto(@RequestBody @Valid Produto produto){   //recebe produto pelo corpo e valida

        Produto objetoProduto = produtoService.salvarProduto(produto);
        return objetoProduto;

        //return produtoService.salvarProduto(produto);
    }

    @GetMapping   //usar o verbo get
    public Iterable<Produto> lerTodosOsProdutos(){
        return produtoService.lerTodosOsProdutos();
    }

    @GetMapping("/{id}")
    public Produto pesquisarPorId(@PathVariable(name = "id") int id){

        try{

            Produto produto = produtoService.buscarProdutoPeloId(id);
            return produto;
        }catch(RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Produto atualizarProduto(@RequestBody Produto produto, @PathVariable(name = "id") int id){

        try{

            Produto produtoDB = produtoService.atualizarProduto(id, produto);
            return produtoDB;
        }catch(RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarProduto(@PathVariable(name = "id") int id){
        try{

            produtoService.deletarProduto(id);

        }catch(RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}

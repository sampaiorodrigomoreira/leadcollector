package br.com.lead.collector.models;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "produtos")
public class Produto {

    @Id  //marcar como chave primaria
    @GeneratedValue(strategy = GenerationType.IDENTITY) //gerado automaticamente pelo banco de dados
    private int id;

    //validacoes
    @NotNull(message = "Não pode ser nulo")
    @NotBlank(message = "Não pode estar em branco") //usado so para string
    @Size(min = 5, message = "Nome no minimo 5 caracteres")
    private String nome;

    @NotNull(message = "Não pode ser nulo")
    @NotBlank(message = "Não pode estar em branco") //usado so para string
    private String descricao;

    @NotNull
    @Digits(integer = 6, fraction = 2, message = "preco fora do padrao")     //casas a esquerda e a direita
    @DecimalMin(value = "1.0", message = "Valor está fora do normal")
    private double preco;

    //obrigatorio
    public Produto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }
}

package br.com.lead.collector.models;

import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

//torna a classe uma tabela no banco
@Entity
//@Table = o hibernate vai procurar a tabela no banco de dados, caso não exista ele cria
//@Table(name = "leads")
public class Lead {

    //cria a chave primaria da tabela
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    //essa anotacao se refere ao nome da coluna no banco de dados
    //@Column(name = "nome_cliente")

    @NotNull(message = "não pode ser nulo")
    @NotBlank(message = "Não pode estar em branco")
    @Size(min = 3, message = "Nome no minimo 3 caracteres")
    private String nome;

    //@CPF(message = "CPF é invalido")
    //@NotNull
    //@Column(name = "cpf_cliente")
    private String cpf;

    @CPF(message = "EMAIL é invalido")
    @NotNull
    private String email;

    private String telefone;

    public Lead() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
